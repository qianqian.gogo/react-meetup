// domain.com/new-meetup
import Head from 'next/head';
import { useRouter } from 'next/router';
import { Fragment } from 'react';
import NewMeetupForm from '../components/meetups/NewMeetupForm';

const  NewMeetupPage = () => {
    const router = useRouter();

    const addMertupHandler = async (meetup) => {
        const response = await fetch('/api/new-meetup', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(meetup)
        });

        const data = await response.json();

        console.log(data);

        router.push('/');
    }

    return (
        <Fragment>
             <Head>
                <title>Add a New Meetup</title>
                <meta
                    name="description" 
                    content="Add your own meetups and create amazing networkig opportunities."
                />
            </Head>
            <NewMeetupForm onAddMeetup={addMertupHandler}/>
        </Fragment>
    );
}

export default NewMeetupPage;