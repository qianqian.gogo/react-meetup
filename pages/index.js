// domain.com/
import Head from 'next/head';
import { MongoClient } from 'mongodb';
import MeetupList from '../components/meetups/MeetupList';
import { Fragment } from 'react';

const HomePage = (props) => {
    return (
        <Fragment>
            <Head>
                <title>React Meetups</title>
                <meta
                    name="description" 
                    content="Browse a huge list of highly active React meetups!"
                />
            </Head>
            <MeetupList meetups={props.meetups}/>
        </Fragment>
    );
}

// NextJS will look for this function with the name and if it finds it, it executes this function during the pre-render process
// the getStaticProps function can be async
// any code inside the function will never end up on the client side, and it will never execute on the client side, simply because this code is executed during the build process, not on the server and especially not on the clients of your visitors
export async function getStaticProps() {
    // fetch data from an API

    // fetch('/api/meetups');
    const client = await MongoClient.connect('mongodb+srv://sola:sola2022!@gogo2022.wuibatq.mongodb.net/meetups?retryWrites=true&w=majority');
    const db = client.db();

    const meetupsCollection = db.collection('meetups');
    const meetups = await meetupsCollection.find().toArray();
    client.close();
    
    // always return an object
    return {
        // the props here will be set as the props for this page component
        props: {
            meetups: meetups.map(meetup => (
                {
                    title: meetup.title,
                    image: meetup.image,
                    address: meetup.address,
                    description: meetup.description,
                    id: meetup._id.toString()
                }
            ))
        },
        // with this revalidate, we unlock a feature called Incremental Static Generation
        // with revalidate set to some number, this page will not just be generated during the build process, but it will also be generated every couple of seconds on the server, at least if there are requests for this page.
        revalidate: 1          // 10 is the number of seconds that NextJS will wait until it regenerates this page for an incoming request. that means that this page, with revalidate set to 10, would be generated on the server at least every 10 seconds if there are requests coming in for this page. and then these regenerated pages will replace the old pre-generated pages.
    };      
}

/**
export const getServerSideProps = (context) => {
    const req = context.req;
    const res = context.res;

    // fetch data from an API

    return {
        props: {
            meetups: DUMMY_MEETUPS
        }
    }
}
 */

export default HomePage;