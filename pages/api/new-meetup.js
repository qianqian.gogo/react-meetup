// POST /api/new-meetup

// if a request is sent to this URL, it will trigger the function which we define in this file
import {MongoClient} from 'mongodb';

async function handler(req, res) {
    if (req.method === 'POST') {
        const data = req.body;
        // const {title, image, address, description} = data;

        const client = await MongoClient.connect('mongodb+srv://sola:sola2022!@gogo2022.wuibatq.mongodb.net/meetups?retryWrites=true&w=majority');
        const db = client.db();

        const meetupsCollection = db.collection('meetups');
        const result = await meetupsCollection.insertOne(data);

        console.log(result);

        client.close();

        res.status(201).json({ message: "Meetup inserted!" });
    }
}

export default handler;

