import { MongoClient, ObjectId } from "mongodb";
import Head from "next/head";
import { Fragment } from "react";
import MeetupDetail from "../../components/meetups/MeetupDetail";

const MeetupDetailPage = (props) => {
    return (
        <Fragment>
            <Head>
                <title>{props.meetupData.title}</title>
                <meta
                    name="description"
                    content={props.meetupData.description}
                />
            </Head>
            <MeetupDetail
                image={props.meetupData.image}
                title={props.meetupData.title}
                address={props.meetupData.address}
                description={props.meetupData.description}
            />
        </Fragment>
    )
}

// we need to pre-generate for all the URLs, for all the meetup ID values users might be entering at runtime
export const getStaticPaths = async () => {
    const client = await MongoClient.connect('mongodb+srv://sola:sola2022!@gogo2022.wuibatq.mongodb.net/meetups?retryWrites=true&w=majority');

    const db = client.db();

    const meetupsCollection = db.collection('meetups');
    // only get Ids
    const meetups = await meetupsCollection.find({}, {_id: 1}).toArray();
    client.close();

    return {
        // false means that we define all the supported path here, if user enters anything that's not supported, he would see a 404 error
        // true or blocking means to tell NextJS that the list of paths which we have specified here might not be exhuasive. NextJS will not respond with a 404 page if it can't find the page immediately. instead it will generate that page on demand and thereafter cache it
        fallback: 'blocking',     // true
        paths: meetups.map(meetup => ({
            params: {
                meetupId: meetup._id.toString(),
            }
        }))
    }
}

export const getStaticProps = async (context) => {
    const meetupId = context.params.meetupId;
    
    const client = await MongoClient.connect('mongodb+srv://sola:sola2022!@gogo2022.wuibatq.mongodb.net/meetups?retryWrites=true&w=majority');

    const db = client.db();

    const meetupsCollection = db.collection('meetups');

    const selectedMeetup = await meetupsCollection.findOne({_id: ObjectId(meetupId)});
    
    client.close();

    return {
        props: {
            meetupData: {
                id: selectedMeetup._id.toString(),
                title: selectedMeetup.title,
                address: selectedMeetup.address,
                image: selectedMeetup.image,
                description: selectedMeetup.description
            }
        }
    }
}

export default MeetupDetailPage;